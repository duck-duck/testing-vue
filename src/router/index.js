import Vue from 'vue'
import Router from 'vue-router'
import SearchProperty from '@/components/SearchProperty'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Search Property',
      component: SearchProperty
    },
  ]
})
